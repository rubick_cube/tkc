#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define yellow 'Y'
#define red 'R'
#define blue 'B'
#define orange 'O'
#define green 'G'
#define white 'W'
#define QUEUESIZE (1024 * 1024)
#define BLOCKS (1024 * 1024)

char queue[24 * QUEUESIZE];
int head = 0, tail = 0;

typedef struct {
  char Ret[24];
} Return;

typedef struct {
  char pos[6][8];
} cube;

char pre[24 * BLOCKS];

int bnum = 0;

void enqueue(char *val);
int dequeue(cube *c);
cube cube_init();
void move(cube *c, int line, int rec);
int match(cube c);
void display(cube c);
int solve(cube c);

void enqueue(char *val) {
  int i;
  if((tail + 1) % QUEUESIZE == head) {
    printf("queue over flow\n");
    exit(1);
  } else {
    for(i = 0; i < 24; i++) {
      queue[24 * tail + i] = val[i];
    }
    tail = (tail + 1) % QUEUESIZE;
  }
}

int dequeue(cube *c) {
  int i;
  if(head == tail) {
    printf("empty\n");
    return -1;
  } else {
    for(i = 2; i < 4; i++) c->pos[i][0] = queue[24 * head + i - 2];
    for(i = 2; i < 4; i++) c->pos[i][1] = queue[24 * head + i];
    for(i = 0; i < 6; i++) c->pos[i][2] = queue[24 * head + i + 4];
    for(i = 0; i < 6; i++) c->pos[i][3] = queue[24 * head + i + 10];
    for(i = 2; i < 4; i++) c->pos[i][4] = queue[24 * head + i + 14];
    for(i = 2; i < 4; i++) c->pos[i][5] = queue[24 * head + i + 16];
    for(i = 2; i < 4; i++) c->pos[i][6] = queue[24 * head + i + 18];
    for(i = 2; i < 4; i++) c->pos[i][7] = queue[24 * head + i + 20];

    head = (head + 1) % QUEUESIZE;
    return 0;
  }
}

cube cube_init() {
  int i, j;
  cube c;

  c.pos[2][0] = white;
  c.pos[3][0] = green;
  c.pos[2][1] = white;
  c.pos[3][1] = green;
  c.pos[0][2] = blue;
  c.pos[1][2] = blue;
  c.pos[2][2] = orange;
  c.pos[3][2] = white;
  c.pos[4][2] = red;
  c.pos[5][2] = red;
  c.pos[0][3] = blue;
  c.pos[1][3] = blue;
  c.pos[2][3] = orange;
  c.pos[3][3] = white;
  c.pos[4][3] = red;
  c.pos[5][3] = red;
  c.pos[2][4] = yellow;
  c.pos[3][4] = orange;
  c.pos[2][5] = yellow;
  c.pos[3][5] = orange;
  c.pos[2][6] = green;
  c.pos[3][6] = yellow;
  c.pos[2][7] = green;
  c.pos[3][7] = yellow;

  /*c.pos[2][0] = red;
  c.pos[3][0] = white;
  c.pos[2][1] = yellow;
  c.pos[3][1] = white;
  c.pos[0][2] = blue;
  c.pos[1][2] = orange;
  c.pos[2][2] = blue;
  c.pos[3][2] = orange;
  c.pos[4][2] = green;
  c.pos[5][2] = blue;
  c.pos[0][3] = red;
  c.pos[1][3] = red;
  c.pos[2][3] = green;
  c.pos[3][3] = white;
  c.pos[4][3] = blue;
  c.pos[5][3] = orange;
  c.pos[2][4] = white;
  c.pos[3][4] = red;
  c.pos[2][5] = yellow;
  c.pos[3][5] = yellow;
  c.pos[2][6] = green;
  c.pos[3][6] = green;
  c.pos[2][7] = yellow;
  c.pos[3][7] = orange;*/

  /*c.pos[2][0] = yellow;
  c.pos[3][0] = green;
  c.pos[2][1] = red;
  c.pos[3][1] = orange;
  c.pos[0][2] = orange;
  c.pos[1][2] = green;
  c.pos[2][2] = yellow;
  c.pos[3][2] = white;
  c.pos[4][2] = blue;
  c.pos[5][2] = red;
  c.pos[0][3] = blue;
  c.pos[1][3] = red;
  c.pos[2][3] = blue;
  c.pos[3][3] = white;
  c.pos[4][3] = blue;
  c.pos[5][3] = green;
  c.pos[2][4] = yellow;
  c.pos[3][4] = red;
  c.pos[2][5] = orange;
  c.pos[3][5] = white;
  c.pos[2][6] = yellow;
  c.pos[3][6] = orange;
  c.pos[2][7] = green;
  c.pos[3][7] = white;*/

  /*  c.pos[2][0] = red;
  c.pos[3][0] = red;
  c.pos[2][1] = red;
  c.pos[3][1] = white;
  c.pos[0][2] = yellow;
  c.pos[1][2] = yellow;
  c.pos[2][2] = blue;
  c.pos[3][2] = blue;
  c.pos[4][2] = orange;
  c.pos[5][2] = white;
  c.pos[0][3] = yellow;
  c.pos[1][3] = blue;
  c.pos[2][3] = orange;
  c.pos[3][3] = white;
  c.pos[4][3] = blue;
  c.pos[5][3] = white;
  c.pos[2][4] = yellow;
  c.pos[3][4] = red;
  c.pos[2][5] = orange;
  c.pos[3][5] = orange;
  c.pos[2][6] = green;
  c.pos[3][6] = green;
  c.pos[2][7] = green;
  c.pos[3][7] = green;*/

  /* c.pos[2][0] = ;
  c.pos[3][0] = ;
  c.pos[2][1] = ;
  c.pos[3][1] = ;
  c.pos[0][2] = ;
  c.pos[1][2] = ;
  c.pos[2][2] = ;
  c.pos[3][2] = ;
  c.pos[4][2] = ;
  c.pos[5][2] = ;
  c.pos[0][3] = ;
  c.pos[1][3] = ;
  c.pos[2][3] = ;
  c.pos[3][3] = ;
  c.pos[4][3] = ;
  c.pos[5][3] = ;
  c.pos[2][4] = ;
  c.pos[3][4] = ;
  c.pos[2][5] = ;
  c.pos[3][5] = ;
  c.pos[2][6] = ;
  c.pos[3][6] = ;
  c.pos[2][7] = ;
  c.pos[3][7] = ;*/

  for(i = 2; i < 4; i++) pre[24 * bnum + i - 2] = c.pos[i][0];
  for(i = 2; i < 4; i++) pre[24 * bnum + i] = c.pos[i][1];
  for(i = 0; i < 6; i++) pre[24 * bnum + i + 4] = c.pos[i][2];
  for(i = 0; i < 6; i++) pre[24 * bnum + i + 10] = c.pos[i][3];
  for(i = 2; i < 4; i++) pre[24 * bnum + i + 14] = c.pos[i][4];
  for(i = 2; i < 4; i++) pre[24 * bnum + i + 16] = c.pos[i][5];
  for(i = 2; i < 4; i++) pre[24 * bnum + i + 18] = c.pos[i][6];
  for(i = 2; i < 4; i++) pre[24 * bnum + i + 20] = c.pos[i][7];
  display(c);
  return c;
}

void move(cube *c, int line, int rec) {
  //  char line , direction;
  char temp[2];
  int i;

  /*  printf("line(A or B or C):");
      scanf("%s", &line);*/
  if(line == 0) {
    //printf("A\n");
    /*    printf("direction(U or D):");
    scanf("%s", &direction);
    if(direction == 'U') {*/
    temp[0] = c->pos[2][0];
    temp[1] = c->pos[2][1];
    for(i = 0; i < 6; i++) c->pos[2][i] = c->pos[2][i + 2];
    for(i = 0; i < 2; i++) c->pos[2][i + 6] = temp[i];
    temp[0] = c->pos[0][2];
    c->pos[0][2] = c->pos[1][2];
    c->pos[1][2] = c->pos[1][3];
    c->pos[1][3] = c->pos[0][3];
    c->pos[0][3] = temp[0];
  } else if(line == 3) {
    temp[0] = c->pos[2][6];
    temp[1] = c->pos[2][7];
    for(i = 7; i > 1; i--) c->pos[2][i] = c->pos[2][i - 2];
    for(i = 0; i < 2; i++) c->pos[2][i] = temp[i];
    temp[0] = c->pos[0][2];
    c->pos[0][2] = c->pos[0][3];
    c->pos[0][3] = c->pos[1][3];
    c->pos[1][3] = c->pos[1][2];
    c->pos[1][2] = temp[0];
    /*} else {
      printf("%c:", direction);
      printf("error\n");
      exit(1);
      }*/
  } else if(line == 1) {
    //printf("B\n");
    /* printf("direction(U or D):");
    scanf("%s", &direction);
    if(direction == 'U') {*/
    for(i = 2; i < 4; i++) {
      temp[i - 2] = c->pos[5][5 - i];
      c->pos[5][5 - i] = c->pos[i][5];
      c->pos[i][5] = c->pos[0][i];
      c->pos[0][i] = c->pos[5 - i][0];
      c->pos[5 - i][0] = temp[i - 2];
    }
    temp[0] = c->pos[2][6];
    c->pos[2][6] = c->pos[2][7];
    c->pos[2][7] = c->pos[3][7];
    c->pos[3][7] = c->pos[3][6];
    c->pos[3][6] = temp[0];
  } else if(line == 4) {
    for(i = 2; i < 4; i++) {
      temp[i - 2] = c->pos[5][i];
      c->pos[5][i] = c->pos[i][0];
      c->pos[i][0] = c->pos[0][5 - i];
      c->pos[0][5 - i] = c->pos[5 - i][5];
      c->pos[5 - i][5] = temp[i - 2];
    }
    temp[0] = c->pos[2][6];
    c->pos[2][6] = c->pos[3][6];
    c->pos[3][6] = c->pos[3][7];
    c->pos[3][7] = c->pos[2][7];
    c->pos[2][7] = temp[0];
    /*} else {
      printf("%c:", direction);
      printf("error\n");
      exit(1);
      }*/
  } else if(line == 2) {
    //printf("C\n");
    /*    printf("direction(R or L):");
    scanf("%s", &direction);
    if(direction == 'R') {*/
    temp[0] = c->pos[4][2];
    temp[1] = c->pos[5][2];
    for(i = 5; i > 1; i--) c->pos[i][2] = c->pos[i - 2][2];
    for(i = 2; i < 4; i++) {
      c->pos[3 - i][2] = c->pos[i][7];
      c->pos[i][7] = temp[3 - i];
    }
    temp[0] = c->pos[2][0];
    c->pos[2][0] = c->pos[3][0];
    c->pos[3][0] = c->pos[3][1];
    c->pos[3][1] = c->pos[2][1];
    c->pos[2][1] = temp[0];
  } else if(line == 5) {
    temp[0] = c->pos[0][2];
    temp[1] = c->pos[1][2];
    for(i = 0; i < 4; i++) c->pos[i][2] = c->pos[i + 2][2];
    for(i = 4; i < 6; i++) {
      c->pos[i][2] = c->pos[7 - i][7];
      c->pos[7 - i][7] = temp[i - 4];
    }
    temp[0] = c->pos[2][0];
    c->pos[2][0] = c->pos[2][1];
    c->pos[2][1] = c->pos[3][1];
    c->pos[3][1] = c->pos[3][0];
    c->pos[3][0] = temp[0];
    /*} else {
      printf("%c:", direction);
      printf("error\n");
      exit(1);
      }*/
  }
  if(rec) {
    bnum++;
    for(i = 2; i < 4; i++) pre[24 * bnum + i - 2] = c->pos[i][0];
    for(i = 2; i < 4; i++) pre[24 * bnum + i] = c->pos[i][1];
    for(i = 0; i < 6; i++) pre[24 * bnum + i + 4] = c->pos[i][2];
    for(i = 0; i < 6; i++) pre[24 * bnum + i + 10] = c->pos[i][3];
    for(i = 2; i < 4; i++) pre[24 * bnum + i + 14] = c->pos[i][4];
    for(i = 2; i < 4; i++) pre[24 * bnum + i + 16] = c->pos[i][5];
    for(i = 2; i < 4; i++) pre[24 * bnum + i + 18] = c->pos[i][6];
    for(i = 2; i < 4; i++) pre[24 * bnum + i + 20] = c->pos[i][7];
    if(bnum % 10000 == 0) {
      printf("block:%d\n", bnum);
      // display(*c);
    }
  }
}

int match(cube c) {
  if(c.pos[2][0] == c.pos[3][0]) 
    if(c.pos[2][1] == c.pos[3][1]) 
      if(c.pos[2][0] == c.pos[2][1]) 
	if(c.pos[0][2] == c.pos[1][2]) 
	  if(c.pos[0][3] == c.pos[1][3])
	    if(c.pos[0][2] == c.pos[0][3])
	      if(c.pos[2][2] == c.pos[2][3])
		if(c.pos[3][2] == c.pos[3][3])
		  if(c.pos[2][2] == c.pos[3][2])
		    if(c.pos[4][2] == c.pos[4][3])
		      if(c.pos[5][2] == c.pos[5][3])
			if(c.pos[4][2] == c.pos[5][2])
			  if(c.pos[2][4] == c.pos[3][4])
			    if(c.pos[2][5] == c.pos[3][5])
			      if(c.pos[2][4] == c.pos[2][5])
				return 1;
  return 0;
}

void display(cube c) {
  int i, j;
  printf("\n  A  B\n\n  ");
  for(i = 2; i < 4; i++) printf("%c", c.pos[i][0]);
  printf("\n  ");
  for(i = 2; i < 4; i++) printf("%c", c.pos[i][1]);
  printf("\n");
  for(j = 2; j < 4; j++) {
    for(i = 0; i < 6; i++) printf("%c", c.pos[i][j]);
    printf(" |");
    for(i = 3; i > 1; i--) printf("%c", c.pos[i][9 - j]);
    if(j == 2) printf("  --C");
    printf("\n");
  }
  printf("  ");
  for(j = 4; j < 8; j++) {
    for(i = 2; i < 4; i++) printf("%c", c.pos[i][j]);
    printf("\n  ");
  }
  printf("\n");
}

int solve(cube c) {
  int i, j, k, same;
  while(dequeue(&c) != -1) {
    for(i = 0; i < 3; i++) {
      same = 0;
      move(&c, i, 1);
      if(match(c)) {
	display(c);
	return 0;
      }
      for(j = 0; j < bnum; j++) {
	//for(k = 0; k < 24; k++) printf("%c", pre[24 * j + k]);
	//printf("\n");
	//for(k = 0; k < 24; k++) printf("%c", pre[24 * bnum + k]);
	//printf("\n");
	if(strncmp(&pre[24 * j], &pre[24 * bnum], 24) == 0) {
	  bnum--;
	  //printf("j:%d\n", j);
	  same = 1;
	  //printf("\n___________\n");
	  move(&c, i+3, 0);
	  break;
	}
      }
      if(same) continue;
      enqueue(&pre[24 * bnum]);
      move(&c, i+3, 0);
    }
  }
  return 0;
}

int main() {
  cube c;
  int i, j, k;
  c = cube_init();
  enqueue(pre);
  solve(c);//display(c);
  printf("\n");
  return 0;
}


