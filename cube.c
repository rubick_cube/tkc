#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define yellow 'Y'
#define red 'R'
#define blue 'B'
#define orange 'O'
#define green 'G'
#define white 'W'
#define BLOCKS 1024 * 1024

typedef struct {
  char pos[6][8];
} cube;

char pre[24 * BLOCKS];

int bnum = 0;

cube cube_init();
void move(cube *c, int line);
int match(cube c);
void display(cube c);
int solve(cube c, int depth);

cube cube_init() {
  int i, j;
  cube c;

  /*c.pos[2][0] = white;
  c.pos[3][0] = green;
  c.pos[2][1] = white;
  c.pos[3][1] = green;
  c.pos[0][2] = blue;
  c.pos[1][2] = blue;
  c.pos[2][2] = orange;
  c.pos[3][2] = white;
  c.pos[4][2] = red;
  c.pos[5][2] = red;
  c.pos[0][3] = blue;
  c.pos[1][3] = blue;
  c.pos[2][3] = orange;
  c.pos[3][3] = white;
  c.pos[4][3] = red;
  c.pos[5][3] = red;
  c.pos[2][4] = yellow;
  c.pos[3][4] = orange;
  c.pos[2][5] = yellow;
  c.pos[3][5] = orange;
  c.pos[2][6] = green;
  c.pos[3][6] = yellow;
  c.pos[2][7] = green;
  c.pos[3][7] = yellow;*/

  /*c.pos[2][0] = red;
  c.pos[3][0] = white;
  c.pos[2][1] = yellow;
  c.pos[3][1] = white;
  c.pos[0][2] = blue;
  c.pos[1][2] = orange;
  c.pos[2][2] = blue;
  c.pos[3][2] = orange;
  c.pos[4][2] = green;
  c.pos[5][2] = blue;
  c.pos[0][3] = red;
  c.pos[1][3] = red;
  c.pos[2][3] = green;
  c.pos[3][3] = white;
  c.pos[4][3] = blue;
  c.pos[5][3] = orange;
  c.pos[2][4] = white;
  c.pos[3][4] = red;
  c.pos[2][5] = yellow;
  c.pos[3][5] = yellow;
  c.pos[2][6] = green;
  c.pos[3][6] = green;
  c.pos[2][7] = yellow;
  c.pos[3][7] = orange;*/

  c.pos[2][0] = yellow;
  c.pos[3][0] = green;
  c.pos[2][1] = red;
  c.pos[3][1] = orange;
  c.pos[0][2] = orange;
  c.pos[1][2] = green;
  c.pos[2][2] = yellow;
  c.pos[3][2] = white;
  c.pos[4][2] = blue;
  c.pos[5][2] = red;
  c.pos[0][3] = blue;
  c.pos[1][3] = red;
  c.pos[2][3] = blue;
  c.pos[3][3] = white;
  c.pos[4][3] = blue;
  c.pos[5][3] = green;
  c.pos[2][4] = yellow;
  c.pos[3][4] = red;
  c.pos[2][5] = orange;
  c.pos[3][5] = white;
  c.pos[2][6] = yellow;
  c.pos[3][6] = orange;
  c.pos[2][7] = green;
  c.pos[3][7] = white;
  move(&c, 0);

  for(i = 2; i < 4; i++) pre[24 * bnum + i - 2] = c.pos[i][0];
  for(i = 2; i < 4; i++) pre[24 * bnum + i] = c.pos[i][1];
  for(i = 0; i < 6; i++) pre[24 * bnum + i + 4] = c.pos[i][2];
  for(i = 0; i < 6; i++) pre[24 * bnum + i + 10] = c.pos[i][3];
  for(i = 2; i < 4; i++) pre[24 * bnum + i + 14] = c.pos[i][4];
  for(i = 2; i < 4; i++) pre[24 * bnum + i + 16] = c.pos[i][5];
  for(i = 2; i < 4; i++) pre[24 * bnum + i + 18] = c.pos[i][6];
  for(i = 2; i < 4; i++) pre[24 * bnum + i + 20] = c.pos[i][7];
  display(c);
  return c;
}

void move(cube *c, int line) {
  //  char line , direction;
  char temp[2];
  int i;

  /*  printf("line(A or B or C):");
      scanf("%s", &line);*/
  if(line == 0) {
    //printf("A\n");
    /*    printf("direction(U or D):");
    scanf("%s", &direction);
    if(direction == 'U') {*/
    temp[0] = c->pos[2][0];
    temp[1] = c->pos[2][1];
    for(i = 0; i < 6; i++) c->pos[2][i] = c->pos[2][i + 2];
    for(i = 0; i < 2; i++) c->pos[2][i + 6] = temp[i];
    temp[0] = c->pos[0][2];
    c->pos[0][2] = c->pos[1][2];
    c->pos[1][2] = c->pos[1][3];
    c->pos[1][3] = c->pos[0][3];
    c->pos[0][3] = temp[0];
  } else if(line == 3) {
    temp[0] = c->pos[2][6];
    temp[1] = c->pos[2][7];
    for(i = 7; i > 1; i--) c->pos[2][i] = c->pos[2][i - 2];
    for(i = 0; i < 2; i++) c->pos[2][i] = temp[i];
    temp[0] = c->pos[0][2];
    c->pos[0][2] = c->pos[0][3];
    c->pos[0][3] = c->pos[1][3];
    c->pos[1][3] = c->pos[1][2];
    c->pos[1][2] = temp[0];
    /*} else {
      printf("%c:", direction);
      printf("error\n");
      exit(1);
      }*/
  } else if(line == 1) {
    //printf("B\n");
    /* printf("direction(U or D):");
    scanf("%s", &direction);
    if(direction == 'U') {*/
    for(i = 2; i < 4; i++) {
      temp[i - 2] = c->pos[5][5 - i];
      c->pos[5][5 - i] = c->pos[i][5];
      c->pos[i][5] = c->pos[0][i];
      c->pos[0][i] = c->pos[5 - i][0];
      c->pos[5 - i][0] = temp[i - 2];
    }
    temp[0] = c->pos[2][6];
    c->pos[2][6] = c->pos[2][7];
    c->pos[2][7] = c->pos[3][7];
    c->pos[3][7] = c->pos[3][6];
    c->pos[3][6] = temp[0];
  } else if(line == 4) {
    for(i = 2; i < 4; i++) {
      temp[i - 2] = c->pos[5][i];
      c->pos[5][i] = c->pos[i][0];
      c->pos[i][0] = c->pos[0][5 - i];
      c->pos[0][5 - i] = c->pos[5 - i][5];
      c->pos[5 - i][5] = temp[i - 2];
    }
    temp[0] = c->pos[2][6];
    c->pos[2][6] = c->pos[3][6];
    c->pos[3][6] = c->pos[3][7];
    c->pos[3][7] = c->pos[2][7];
    c->pos[2][7] = temp[0];
    /*} else {
      printf("%c:", direction);
      printf("error\n");
      exit(1);
      }*/
  } else if(line == 2) {
    //printf("C\n");
    /*    printf("direction(R or L):");
    scanf("%s", &direction);
    if(direction == 'R') {*/
    temp[0] = c->pos[4][2];
    temp[1] = c->pos[5][2];
    for(i = 5; i > 1; i--) c->pos[i][2] = c->pos[i - 2][2];
    for(i = 2; i < 4; i++) {
      c->pos[3 - i][2] = c->pos[i][7];
      c->pos[i][7] = temp[i - 2];
    }
    temp[0] = c->pos[2][0];
    c->pos[2][0] = c->pos[3][0];
    c->pos[3][0] = c->pos[3][1];
    c->pos[3][1] = c->pos[2][1];
    c->pos[2][1] = temp[0];
  } else if(line == 5) {
    temp[0] = c->pos[0][2];
    temp[1] = c->pos[1][2];
    for(i = 0; i < 4; i++) c->pos[i][2] = c->pos[i + 2][2];
    for(i = 4; i < 6; i++) {
      c->pos[i][2] = c->pos[7 - i][7];
      c->pos[7 - i][7] = temp[i - 4];
    }
    temp[0] = c->pos[2][0];
    c->pos[2][0] = c->pos[2][1];
    c->pos[2][1] = c->pos[3][1];
    c->pos[3][1] = c->pos[3][0];
    c->pos[3][0] = temp[0];
    /*} else {
      printf("%c:", direction);
      printf("error\n");
      exit(1);
      }*/
  }
  bnum++;
  for(i = 2; i < 4; i++) pre[24 * bnum + i - 2] = c->pos[i][0];
  for(i = 2; i < 4; i++) pre[24 * bnum + i] = c->pos[i][1];
  for(i = 0; i < 6; i++) pre[24 * bnum + i + 4] = c->pos[i][2];
  for(i = 0; i < 6; i++) pre[24 * bnum + i + 10] = c->pos[i][3];
  for(i = 2; i < 4; i++) pre[24 * bnum + i + 14] = c->pos[i][4];
  for(i = 2; i < 4; i++) pre[24 * bnum + i + 16] = c->pos[i][5];
  for(i = 2; i < 4; i++) pre[24 * bnum + i + 18] = c->pos[i][6];
  for(i = 2; i < 4; i++) pre[24 * bnum + i + 20] = c->pos[i][7];
  if(bnum % 10000 == 0) printf("block:%d\n", bnum);
}

int match(cube c) {
  if(c.pos[2][0] == c.pos[3][0]) 
    if(c.pos[2][1] == c.pos[3][1]) 
      if(c.pos[2][0] == c.pos[2][1]) 
	if(c.pos[0][2] == c.pos[1][2]) 
	  if(c.pos[0][3] == c.pos[1][3])
	    if(c.pos[0][2] == c.pos[0][3])
	      if(c.pos[2][2] == c.pos[2][3])
		if(c.pos[3][2] == c.pos[3][3])
		  if(c.pos[2][2] == c.pos[3][2])
		    if(c.pos[4][2] == c.pos[4][3])
		      if(c.pos[5][2] == c.pos[5][3])
			if(c.pos[4][2] == c.pos[5][2])
			  if(c.pos[2][4] == c.pos[3][4])
			    if(c.pos[2][5] == c.pos[3][5])
			      if(c.pos[2][4] == c.pos[2][5])
				return 1;
  return 0;
}

void display(cube c) {
  int i, j;
  printf("\n  A  B\n\n  ");
  for(i = 2; i < 4; i++) printf("%c", c.pos[i][0]);
  printf("\n  ");
  for(i = 2; i < 4; i++) printf("%c", c.pos[i][1]);
  printf("\n");
  for(j = 2; j < 4; j++) {
    for(i = 0; i < 6; i++) printf("%c", c.pos[i][j]);
    printf(" |");
    for(i = 3; i > 1; i--) printf("%c", c.pos[i][9 - j]);
    if(j == 2) printf("  --C");
    printf("\n");
  }
  printf("  ");
  for(j = 4; j < 8; j++) {
    for(i = 2; i < 4; i++) printf("%c", c.pos[i][j]);
    printf("\n  ");
  }
  printf("\n");
}

int solve(cube c, int depth) {
  int i, j, k, same;
  //printf("depth:%d\n", depth);
  if(match(c)) {
    display(c);
    return 1;
  } else {
    for(i = 0; i < 3; i++) {
      same = 0;
      move(&c, i);
      //display(c);
      for(j = bnum - 1; j >= 0; j--) {
	//for(k = 0; k < 24; k++) printf("%c", pre[24 * j + k]);
	//printf("\n");
	//for(k = 0; k < 24; k++) printf("%c", pre[24 * bnum + k]);
	//printf("\n");
	if(strncmp(&pre[24 * j], &pre[24 * bnum], 24) == 0) {
	  bnum--;
	  //printf("j:%d\n", j);
	  same = 1;
	  //printf("\n___________\n");
	  move(&c, i+3);
	  break;
	}
      }
      if(same) continue;
      if(solve(c, depth+1)) {
	printf("%d", i);
	return 1;
      }
    }
  }
  return 0;
}

int main() {
  cube c;
  int depth = 0;
  c = cube_init();
  if(solve(c, depth+1)) ;//display(c);
  printf("\n");
  return 0;
}
